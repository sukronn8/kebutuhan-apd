export const state = () => ({
  location: {}
})

export const getters = {
  location: (state) => {
    return state.location
  }
}

export const mutations = {
  SET_LOCATION: (state, data) => {
    state.location = data
  }
}

export const actions = {}
